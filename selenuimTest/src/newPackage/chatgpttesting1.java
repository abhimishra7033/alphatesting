import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class AmazonSearchSortByFeature {
    public static void main(String[] args) {
        // Set the path to the Chrome driver executable
        System.setProperty("webdriver.chrome.driver", "path/to/chromedriver.exe");

        // Initialize a new instance of the Chrome driver
        WebDriver driver = new ChromeDriver();

        // Navigate to the Amazon website
        driver.get("https://www.amazon.com/");

        // Find the search input element and enter the search keyword
        WebElement searchInput = driver.findElement(By.id("twotabsearchtextbox"));
        searchInput.sendKeys("laptop");

        // Find the search button and click it
        WebElement searchButton = driver.findElement(By.id("nav-search-submit-button"));
        searchButton.click();

        // Find the 'Sort by' dropdown and click it
        WebElement sortByDropdown = driver.findElement(By.id("s-result-sort-select"));
        sortByDropdown.click();

        // Find the 'Featured' option in the dropdown and click it
        WebElement featuredOption = driver.findElement(By.xpath("//option[text()='Featured']"));
        featuredOption.click();

        // Wait for the page to reload with the sorted results
        try {
         //   Thread.sleep(3000);
        } catch (InterruptedException e) {
        //    e.printStackTrace();
        }

        // Close the browser
        //driver.quit();
    }
}
